!=============================================================
PROGRAM  LANMASK
!=============================================================
!/bffs01/group/intern/users/ples_ad/SRC/CCI/REPROCESSING/tsvm/TSVM_REPROCESSOR_1M/src
!
! MATRIX:  LANDMASK y,x - 720,2160  ymin=-60.0+0.08333, xmin=-180.0+0.08333, dx=dy=0.166666666
!=============================================================
integer LM(720,2160)
integer x,y,xx,yy, mask,i_wv1,i_wv2, ii_wv1,ii_wv2
real    in_lat_y,in_lon_x
real dx,dy ,SWH_NEW

integer IO
real  LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw
integer key,wv1_wv2,  s_date,s_time,e_date,e_time,orbit,des_asc
character scene_ID*70,  s_time_char*6, e_time_char*6,IDN*72
!============================================================= 
	dx=0.16666666
	dy=0.16666666
!============================================================= 
	i_wv1=0
	i_wv2=0
	ii_wv1=0
	ii_wv2=0

	open(555,file='../settings_and_headers/landmask_dydy_0-166666__-60_LAT_60.txt')
		read(555,*)((LM(y,x),x=1,2160),y=1,720)
	close(555)
!=============================================================================
!  wv-1 
!=============================================================================
	open(11, file= '../tmp/tmp_results/results_wv1_land-cheked.txt')
	open(1,  file= '../tmp/tmp_results/results_wv1.txt')

	do 100
	read(1,999,IOSTAT=IO )LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw,key,wv1_wv2,s_date,s_time,e_date,e_time,scene_ID,orbit,des_asc,IDN  ! read _all-parameters_seastate.txt with ID
	if (IO /= 0)exit 

	if(key==-10)SWH=0.0 

	yy = ( LAT - ( -60.) )/dy 
	xx = ( LON - (-180.) )/dx 
		if((yy.gt.30).and.(yy.lt.720 ))then 
		if((xx.gt.1 ).and.(xx.lt.2160))then
			!write(*,*) LAT,LON,yy,xx, LM(yy,xx)
		mask=9
		if(LM(yy-1,xx-1)==1) mask=mask-1
		if(LM(yy-1,xx  )==1) mask=mask-1
		if(LM(yy-1,xx+1)==1) mask=mask-1
		
		if(LM(yy  ,xx-1)==1) mask=mask-1
		if(LM(yy  ,xx  )==1) mask=mask-1
		if(LM(yy  ,xx+1)==1) mask=mask-1
		
		if(LM(yy+1,xx-1)==1) mask=mask-1
		if(LM(yy+1,xx  )==1) mask=mask-1
		if(LM(yy+1,xx+1)==1) mask=mask-1
		
	if(mask.lt.1) then 
		 i_wv1 = i_wv1 +1
		! write(*,*)i_wv1, LAT,LON,yy,xx, LM(yy,xx),key,LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw,key,wv1_wv2 
		SWH=0.0
		Tm0=0.0
		Tm1=0.0
		Tm2=0.0
		SW1=0.0
		SW2=0.0
		SWW=0.0
		Tmw=0.0
		if(key.ne.-10) then 
			ii_wv1=ii_wv1+1
			!write(*,*)ii_wv1,i_wv1, LAT,LON,yy,xx, LM(yy,xx),key,LAT,LON,SWH,wv1_wv2
		end if 
		key = -10
	 end if 
				
	end if 
	end if
    

	if                       (s_time.gt.99999) write(s_time_char,   '(i6)')        s_time
	if((s_time.le.99999).and.(s_time.gt. 9999))write(s_time_char,'(a1,i5)')'0'    ,s_time
	if((s_time.le.9999 ).and.(s_time.gt.  999))write(s_time_char,'(a2,i4)')'00'   ,s_time
	if((s_time.le.999  ).and.(s_time.gt.   99))write(s_time_char,'(a3,i3)')'000'  ,s_time
	if((s_time.le.99   ).and.(s_time.gt.    9))write(s_time_char,'(a4,i2)')'0000' ,s_time
	if (s_time.le.9    )                       write(s_time_char,'(a5,i1)')'00000',s_time

	if                       (e_time.gt.99999) write(e_time_char,   '(i6)')        e_time
	if((e_time.le.99999).and.(e_time.gt. 9999))write(e_time_char,'(a1,i5)')'0'    ,e_time
	if((e_time.le.9999 ).and.(e_time.gt.  999))write(e_time_char,'(a2,i4)')'00'   ,e_time
	if((e_time.le.999  ).and.(e_time.gt.   99))write(e_time_char,'(a3,i3)')'000'  ,e_time
	if((e_time.le.99   ).and.(e_time.gt.    9))write(e_time_char,'(a4,i2)')'0000' ,e_time
	if (e_time.le.9    )                       write(e_time_char,'(a5,i1)')'00000',e_time

	! write(*,*)'       time 1:'
	! write(*,*)'      s_time:',s_time
	! write(*,*)' s_time_char:',s_time_char
	! write(*,*)' e_time_char:',s_time_char
	 
	 
write(11,998)LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw,key,wv1_wv2,s_date,s_time_char,e_date,e_time_char,scene_ID,orbit,des_asc,IDN   

      
100 continue 
999 FORMAT(10F12.5,6I12,a70,a10,a14,a72)
998 FORMAT(10F12.5,2I12,i12,a12,i12,a12,a70,a10,a14,a72)

	close(1)
	close(11)	

!=============================================================================
!  wv-2 
!=============================================================================
	open(22, file= '../tmp/tmp_results/results_wv2_land-cheked.txt')
	open(2,  file= '../tmp/tmp_results/results_wv2.txt')

	do 200
	read(2,999,IOSTAT=IO )LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw,key,wv1_wv2,s_date,s_time,e_date,e_time,scene_ID,orbit,des_asc,IDN ! read _all-parameters_seastate.txt with ID
	if (IO /= 0)exit 

	if(key==-10)SWH=0.0 

	yy = ( LAT - ( -60.) )/dy 
	xx = ( LON - (-180.) )/dx 
	if((yy.gt.30).and.(yy.lt.720 ))then   !!   LAT= -60 + dy*y = -60 + 0.16666666*30 = -55 
	if((xx.gt.1 ).and.(xx.lt.2160))then
		!write(*,*) LAT,LON,yy,xx, LM(yy,xx)
		mask=9
		if(LM(yy-1,xx-1)==1) mask=mask-1
		if(LM(yy-1,xx  )==1) mask=mask-1
		if(LM(yy-1,xx+1)==1) mask=mask-1
		
		if(LM(yy  ,xx-1)==1) mask=mask-1
		if(LM(yy  ,xx  )==1) mask=mask-1
		if(LM(yy  ,xx+1)==1) mask=mask-1
		
		if(LM(yy+1,xx-1)==1) mask=mask-1
		if(LM(yy+1,xx  )==1) mask=mask-1
		if(LM(yy+1,xx+1)==1) mask=mask-1
		
		if(mask.lt.1) then 
		 i_wv2 = i_wv2 +1
		! write(*,*)i_wv1, LAT,LON,yy,xx, LM(yy,xx),key,LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw,key,wv1_wv2 
		SWH=0.0
		Tm0=0.0
		Tm1=0.0
		Tm2=0.0
		SW1=0.0
		SW2=0.0
		SWW=0.0
		Tmw=0.0
		if(key.ne.-10) then
			ii_wv2=ii_wv2+1
		 	!write(*,*)ii_wv2,i_wv2, LAT,LON,yy,xx, LM(yy,xx),LM(yy+1,xx),LM(yy-1,xx),key,LAT,LON,SWH,wv1_wv2
		end if 
		key = -10
		end if 

	 end if 
	 end if

	if                       (s_time.gt.99999) write(s_time_char,   '(i6)')        s_time
	if((s_time.le.99999).and.(s_time.gt. 9999))write(s_time_char,'(a1,i5)')'0'    ,s_time
	if((s_time.le.9999 ).and.(s_time.gt.  999))write(s_time_char,'(a2,i4)')'00'   ,s_time
	if((s_time.le.999  ).and.(s_time.gt.   99))write(s_time_char,'(a3,i3)')'000'  ,s_time
	if((s_time.le.99   ).and.(s_time.gt.    9))write(s_time_char,'(a4,i2)')'0000' ,s_time
	if (s_time.le.9    )                       write(s_time_char,'(a5,i1)')'00000',s_time
 
	if                       (e_time.gt.99999) write(e_time_char,   '(i6)')        e_time
	if((e_time.le.99999).and.(e_time.gt. 9999))write(e_time_char,'(a1,i5)')'0'    ,e_time
	if((e_time.le.9999 ).and.(e_time.gt.  999))write(e_time_char,'(a2,i4)')'00'   ,e_time
	if((e_time.le.999  ).and.(e_time.gt.   99))write(e_time_char,'(a3,i3)')'000'  ,e_time
	if((e_time.le.99   ).and.(e_time.gt.    9))write(e_time_char,'(a4,i2)')'0000' ,e_time
	if (e_time.le.9    )                       write(e_time_char,'(a5,i1)')'00000',e_time
	
	
write(22,998)LAT,LON,SWH,Tm0,Tm1,Tm2,SW1,SW2,SWW,Tmw,key,wv1_wv2,s_date,s_time_char,e_date,e_time_char,scene_ID,orbit,des_asc,IDN  

200 continue 
	close(2)
	close(22)	
!=============================================================
	write(*,*)          'LAND MASK CHECK OK                  wv1   wv2'
	write(*,'(a35,2i5)')'ORIGINAL DATA CONTINENT POINTS:', i_wv1, i_wv2
	write(*,'(a35,2i5)')'        ADDITIONAL LAND  FOUND:', ii_wv1, ii_wv2
!=============================================================
STOP
END
!=============================================================
